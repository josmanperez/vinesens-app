# VineSens app

Proyecto para asignatura MUEI APM

## WIKI

En el propio proyecto existe una WIKI donde podemos encontrar las diferentes fases publicadas de la información necesaria para el proyecto. [Acceso directo a la WIKI](https://gitlab.com/josmanperez/vinesens-app/-/wikis/pages)

### Enlaces

* [Análisis de funcionalidades](https://gitlab.com/josmanperez/vinesens-app/-/wikis/An%C3%A1lisis-de-funcionalidades)
* Diseño de la [aquitectura de la aplicación](https://gitlab.com/josmanperez/vinesens-app/-/wikis/Dise%C3%B1o-de-la-arquitectura-de-la-aplicaci%C3%B3n)
* [Estado del arte](https://gitlab.com/josmanperez/vinesens-app/-/wikis/Estado-del-arte)
* [Mockups aplicación](https://gitlab.com/josmanperez/vinesens-app/-/wikis/Mockups-para-aplicaci%C3%B3n)
